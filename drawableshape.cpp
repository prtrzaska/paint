#include "drawableshape.h"


DrawableShape::DrawableShape(ShapeType shape_, bool filled_, float x, float y, float w, float h, ofColor color_){
    shape = shape_;
    filled = filled_;
    xPos = x;
    yPos = y;
    width = w;
    height = h;
    color = color_;
}

void DrawableShape::draw(){
    ofSetColor(color);
    if (filled){
        ofFill();
    }
    else{
        ofNoFill();
    }
    if (shape == ShapeType::rectangle){
        ofDrawRectangle(xPos, yPos, width, height);
    }
    else if (shape ==    ShapeType::ellipse){
        ofDrawEllipse(xPos, yPos, width, height);
    }
}
void DrawableShape::moveTo(float x, float y){
    xPos = x;
    yPos = y;
}
void DrawableShape::moveLeft(){
    xPos -= 10;
}

void DrawableShape::moveRight(){
    xPos += 10;
}
void DrawableShape::moveUp(){
    yPos -= 10;
}
void DrawableShape::moveDown(){
    yPos += 10;
}

void DrawableShape::resize(float h, float w){
    height = h;
    width = w;
}

void DrawableShape::bigger(){
    height *= 1.25;
    width *= 1.25;
}

void DrawableShape::smaller(){
    height *= 0.9;
    width *= 0.9;
}

void DrawableShape::changeColor(ofColor col){
    color = col;
}
