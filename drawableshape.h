#ifndef DRAWABLESHAPE_H
#define DRAWABLESHAPE_H

// we include ofMain.h so we can use the ofColor type
#include "ofMain.h"

// we use an enum to define a fixed set of options for the
// shape type
/**
 * @brief The ShapeType enum - data type to store names of shapes
 */
enum class ShapeType { rectangle, ellipse};


// we are defining a new data structue here called a 'class'
// this class is used to represent shapes that we can draw
// and to provide functions for drawing those shapes
/**
 * @brief The DrawableShape class - a data type to represent a shape and to provide functions for drawing it
 */
class DrawableShape
{
    // public members can be seen from outside
    // so you can access them from your openframeworks
    // function (setup, draw etc.), for example.
    //
public:
    // this is the prototype for the 'constructor'
    // the constructor is called when we create
    // a new DrawableShape
    DrawableShape(ShapeType shape_,
                  bool filled_,
                  float x, float y,
                  float w, float h,
                  ofColor color_);

    // these prototypes are for drawing, moving and resizing

    /**
     * @brief display the shape on the ofx window
     */
    void draw();
    /**
     * @brief move the shape
     * @param x - the new x position
     * @param y - the new y position
     */
    void moveTo(float x, float y);
    /**
     * @brief moveLeft
     */
    void moveLeft();
    /**
     * @brief moveRight
     */
    void moveRight();
    /**
     * @brief moveUp
     */
    void moveUp();
    /**
     * @brief moveDown
     */
    void moveDown();
    /**
     * @brief resize change the size of the shape
     * @param h - the new height
     * @param w - the new width
     */
    void resize(float h, float w);
    /**
     * @brief bigger
     */
    void bigger();
    /**
     * @brief smaller
     */
    void smaller();
    /**
     * @brief changeColor - change the colour of the shape
     * @param col - an ofColor variable representing the new colour
     */
    void changeColor(ofColor col);


    // the private members can only be seen from inside DrawableShape
    // so you cannot see it from your openframeworks ,
    // functions (setup, draw etc.), for example.
   private:

    ShapeType shape;
    float xPos;
    float yPos;
    float height;
    float width;
    ofColor color;
    bool filled;

};

#endif // DRAWABLESHAPE_H
