#include "ofApp.h"
#include "drawableshape.h"

#include <vector>
//--------------------------------------------------------------
void ofApp::setup() {
	ofSetBackgroundAuto(true);
	ofSetBackgroundColor(0);
	brushState = 1;
	ofSetFrameRate(60);

	state = true;
	speed = -2;
	size_X = 33;
	size_Y = 45;
	

}

//--------------------------------------------------------------
void ofApp::update(){
	
	brushState = (brushState + 1) % 4; //recurrence of brush state
	

	
	
	if (state == true) { //just growing constantly
		size_X = size_X - speed;
		size_Y = size_Y - speed;
		if (size_Y > 100 && size_X > 150) { // shrinking when above 49
			size_Y *= -1;
			size_X *= -1;
		}

	} 
	
	if (state == false) {
			
			
		}
	
	//velocity = veloc ity + aceleration;
	//shapes = shapes + velocity;
	 
	

}

//--------------------------------------------------------------
void ofApp::draw(){

	//-----PAINT 2.0 -------

	shape.draw();
	for (DrawableShape shape : shapes) {
		
		shape.draw();
		shapes.back().resize(size_X, size_Y); 
	}


	cout << shapes.size() << endl;




	if (drawing == true) {						//if true do the paintings
		paint(ofGetMouseX(), ofGetMouseY());
	}
	if (erasing == true) {
		erase(ofGetMouseX(), ofGetMouseY());
	}
	if (brushD == true) {
		dynamicBrush(ofGetMouseX(), ofGetMouseY(), brushState);
	}
	if (brushF == true) {
		dynamicBrushForum(ofGetMouseX(), ofGetMouseY(), brushState);
	}




	
		

	

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	x = mouseX; y = mouseY;
	if (key == 'p') { //rectangle shape with nofill();
		drawing = true;
	}
	if (key == 'e') { //eraser rect shape with fill();
		erasing = true;
		shapes.pop_back();


	}
	if (key == 'd') { //dynamic shapes, brush wood imitation
		brushD = true;
	}
	if (key == 'f') { //dynamic shapes, brush from forum
		brushF = true;
	}
	if (key == 's') { //save file in location bin/data
		glReadBuffer(GL_FRONT);								//problem on windows mashine solved! black screen
		img.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
		ofSaveScreen("test.png");
		
	}
	if (key == 'l') { // load file from location bin/data
	
		img.load("test.png");
		img.draw(0, 0,ofGetWidth(),ofGetHeight());
	}
	if (key == 'o') {
		img.load("Original-JPG-Image.jpg");
		img.draw(0, 0, ofGetWidth(), ofGetHeight());
	}

	//---Paint 2.0----
		shape.bigger();

		if (key == 'v') {  // mouse pressed doesnt work so this is alternative method for pushing vectors into array (press V for vectors)
			shapes.push_back(DrawableShape{
			ShapeType::ellipse,
			false,
			x,y, //contain global variables mouseX and mouseY
			50,100,
			ofColor(22,215,255)
				});
		}

		if (key == 57356) { //moving and changing color of last shape from vector array
			shapes.back().changeColor(222);
			shapes.back().moveLeft();
			

		}
		if (key == 57358) { //moving and changing color of last shape from vector array
			shapes.back().changeColor(222);
			shapes.back().moveRight();


		}
		if (key == 'b') { //get smaller
			state = false;
		}
		if (key == 'r') { //gets bigger
			state = true;
			
			
		}

		



}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	
		drawing = false;
		erasing = false;
		brushD = false;
		brushF = false;
		
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(float x, float y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(float x, float y, int button){
	////---Paint 2.0 ----
	//not detected


}


//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
	
}
//................MY FUNCTION DRAWING SIMPLE SHAPES..............
void ofApp::paint(int x, int  y)
{
		ofSetColor(255, 33, 33);
		ofSetRectMode(OF_RECTMODE_CENTER);
		ofDrawRectangle(ofGetMouseX(), ofGetMouseY(), 33, 33);

}
//..............ERASER...............
void ofApp::erase(int x, int y) {
	ofSetColor(0);
	ofFill();
	ofSetRectMode(OF_RECTMODE_CENTER);
	ofDrawRectangle(ofGetMouseX(), ofGetMouseY(), 33, 33);
}
//................IMITATION OF WOODEN CURTAINS...........-->>>>https://learn.gold.ac.uk/mod/forum/user.php?id=60917&course=11569
void ofApp::dynamicBrush(int x, int y, char brushState) {



	int maxRadius = 100;  // Increase for a wider brush
	int radiusStepSize = 1.5;  // Decrease for more circles (i.e. a more opaque brush)
	int alpha = 3;  // Increase for a more opaque brush
	int maxOffsetDistance = 50;  // Increase for a larger spread of circles
	// draw smaller and smaller circles and layering (increasing) opaqueness
	//continuously drawing in for loop circles or beziers. Starts again until radius get 0.
	for (int radius = maxRadius; radius > 0; radius -= radiusStepSize * 2) {
		//randomize angle and distance to make it more interesting
		float angle = ofRandom(ofDegToRad(360.0));
		float distance = ofRandom(maxOffsetDistance);
		float xOffset = cos(angle) * distance;
		float yOffset = sin(angle) * distance;
		ofSetColor(125,ofRandom(100),0, ofRandom(127));
		
		//is spraying paint effect, with distance and radius from each other 
		//ofdrawcircle(ofgetmousex() + xoffset, ofgetmousey() + yoffset, brushstate*radiusstepsize);

		//is drawing Bézier curve with 4 x and y parameters indicating start and end points of the curve
		ofDrawBezier(ofGetMouseX(), ofGetMouseY(), ofGetMouseX() +30, ofGetMouseY() - brushState, ofGetMouseX(), ofGetMouseY() + (yOffset * distance), ofGetMouseX(), ofGetMouseY());
	}
}


//..............FORUM BRUSH......$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ link --->>>https://learn.gold.ac.uk/mod/forum/user.php?id=62441&course=11569
void ofApp::dynamicBrushForum(int x, int y, char brushState) {
	//To set slightly different colors based on the brushState

	//Randomazing the alpha values to make it more realistic

	if (brushState == 0) {

		ofSetColor(145, 254, 255, ofRandom(50));

	}

	else if (brushState == 5) {

		ofSetColor(206, 253, 255, ofRandom(20));

	}

	else if (brushState == 10) {

		ofSetColor(180, 252, 255, ofRandom(40));

	}

	else if (brushState == 15) {

		ofSetColor(190, 251, 255, ofRandom(40, 60));

	}

	unsigned int maxRadius = 20;

	unsigned int steps = 4;

	//To gradually decrease the size of the drawn circles

	for (unsigned int radius = maxRadius; radius > 0; radius -= steps) {



		//Defining offset values to draw them randomly around the mouse position

		float xOffset = ofRandom(-80, 80) * cos(ofRadToDeg(360.0));

		float yOffset = ofRandom(-80, 80) * sin(ofRadToDeg(360.0));



		//To have a kind of "stroke" to it have to draw with fill and noFill at the same time same location

		ofFill();

		ofDrawCircle(x + xOffset, y + yOffset, radius);

		ofNoFill();

		ofDrawCircle(x + xOffset, y + yOffset, radius);

	}



}
