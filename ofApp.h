#pragma once
#include <vector>
#include "ofMain.h"
#include "drawableshape.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed( int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		
		void mouseDragged(float x, float y, int button);
		void mousePressed(float x, float y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		//MY FUNCTIONS
		void paint(int x, int y);
		void erase(int x, int y);
		void dynamicBrush(int x, int y, char brushState);
		void dynamicBrushForum(int x, int y, char brushState);
		
	protected://MY VARIABLES visible in class but not outside of class
		bool drawing;
		bool erasing;
		bool brushD;
		bool brushF;
		char brushState;
		ofImage img;
		ofImage loadimg;
		ofImage grabbed;

		
	private: //visible inside the function not used in this examples
		float x; float y;
		bool state;
		float speed;
		float size_X;
		float size_Y;
		
		DrawableShape shape {
			ShapeType::ellipse,
			false,
			43, 23,
			50,75,
			ofColor(255,255,0)
		};

		std::vector <DrawableShape> shapes;
		std::vector <DrawableShape> velocity;
		std::vector <DrawableShape> aceleration;

		
};
